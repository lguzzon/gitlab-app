#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var server, browser = new Builder().forBrowser('chrome').build();
    var LOCATION = 'test';
    var repodir = '/tmp/testrepo';
    var app, reponame = 'testrepo';
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var TIMEOUT = parseInt(process.env.TIMEOUT) || 40000;
    var gitlab_username = 'root';

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        execSync('chmod o-rw,g-rw id_rsa');

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        rimraf.sync(repodir);
        done();
    });

    function setAdminCredentials(done) {
        browser.get('https://' + app.fqdn).then(function () { // will get redirected to /users/password/edit?reset_password_token=<token>
        }).then(function () {
            return browser.wait(until.elementLocated(by.id('user_password')), 5000);
        }).then(function () {
            return browser.findElement(by.id('user_password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.id('user_password_confirmation')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@value="Change your password"]')).click();
        }).then(function () {
            return browser.wait(function () {
                return browser.getCurrentUrl().then(function (url) {
                    return url === 'https://' + app.fqdn + '/users/sign_in';
                });
            }, TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function adminLogin(tab, done) {
        browser.get('https://' + app.fqdn + '/users/sign_in').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(text(), "' + tab + '")]'), TIMEOUT));
        }).then(function () {
            return browser.findElement(by.xpath('//a[contains(text(), "' + tab + '")]')).click();
        }).then(function () {
            return browser.findElement(by.id('user_login')).sendKeys(gitlab_username);
        }).then(function () {
            return browser.findElement(by.id('user_password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//form[@id="new_user"]//input[@value="Sign in"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(text(), "Projects")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function checkSshCloneUrl(done) {
        browser.get('https://' + app.fqdn + '/' + gitlab_username + '/' + reponame).then(function () {
            return browser.findElement(by.id('project_clone')).getAttribute('value');
        }).then(function (cloneUrl) {
            expect(cloneUrl).to.be('ssh://git@' + app.fqdn + ':29418/' + gitlab_username + '/' + reponame + '.git');
            done();
        });
    }

    function checkHttpCloneUrl(done) {
        browser.get('https://' + app.fqdn + '/' + gitlab_username + '/' + reponame).then(function () {
            return browser.findElement(by.xpath('//a[@class="https-selector"]')).getAttribute('href');
        }).then(function (cloneUrl) {
            expect(cloneUrl).to.be('https://' + app.fqdn + '/' + gitlab_username + '/' + reponame + '.git');
            done();
        });
    }

    function checkClone(done) {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('git clone ssh://git@' + app.fqdn + ':29418/' + gitlab_username + '/' + reponame + '.git ' + repodir, { env: env });
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
        rimraf.sync(repodir);

        env.GIT_SSL_NO_VERIFY = '1';
        execSync('git clone https://' + gitlab_username + ':' + encodeURIComponent(password) + '@' + app.fqdn + '/' + gitlab_username + '/' + reponame + '.git ' + repodir, { env: env });
        expect(fs.existsSync(repodir + '/newfile')).to.be(true);
        rimraf.sync(repodir);

        done();
    }

    function checkLoginAsCloudronUser(done) {
        browser.get('https://' + app.fqdn + '/users/sign_in').then(function () {
            return browser.findElement(by.xpath('//a[contains(text(), "Cloudron")]')).click();
        }).then(function () {
            return browser.findElement(by.id('username')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.id('password')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@value="Sign in"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(text(), "New project")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function logout(done) {
        browser.findElement(by.xpath('//a[@class="header-user-dropdown-toggle"]')).click().then(function() {
            return browser.findElement(by.xpath('//a[@class="sign-out-link"]')).click();
        }).then(function () {
            browser.wait(until.elementLocated(by.xpath('//h3[contains(text(), "Open source software to collaborate on code")]')), TIMEOUT);
        }).then(function() {
            done();
        });
    }

    function addPublicKey(done) {
        browser.get('https://' + app.fqdn + '/profile/keys').then(function () {
            var publicKey = fs.readFileSync(__dirname + '/id_rsa.pub', 'utf8');

            return browser.findElement(by.id('key_key')).sendKeys(publicKey);
        }).then(function () {
            return browser.findElement(by.id('key_title')).sendKeys('testkey');
        }).then(function () {
            return browser.findElement(by.xpath('//input[@value="Add key"]')).click();
        }).then(function () {
            browser.wait(function () {
                return browser.getCurrentUrl().then(function (url) {
                    return url === 'https://' + app.fqdn + '/profile/keys/1';
                });
            }, TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function createRepo(done) {
        browser.get('https://' + app.fqdn + '/projects/new').then(function () {
            return browser.findElement(by.id('project_path')).sendKeys(reponame);
        }).then(function () {
            var button = browser.findElement(by.xpath('//input[@value="Create project"]'));
            return browser.executeScript('arguments[0].scrollIntoView(true)', button);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@value="Create project"]')).click();
        }).then(function () {
            // this can take sometime
            return browser.wait(until.elementLocated(by.xpath('//span[contains(text(), "was successfully created.")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function addReadme(done) {
        browser.get('https://' + app.fqdn + '/' + gitlab_username + '/' + reponame + '/new/master?file_name=README.md').then(function () {
            return browser.findElement(by.xpath('//textarea')).sendKeys('This is a test document');
        }).then(function () {
            return browser.findElement(by.xpath('//textarea[@name="commit_message"]')).sendKeys('This is a test message');
        }).then(function () {
            return browser.findElement(by.xpath('//button[text()="Commit changes"]')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//span[contains(text(), "The file has been successfully created")]')), TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function cloneUrl(done) {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('git clone ssh://git@' + app.fqdn + ':29418/' + gitlab_username + '/' + reponame + '.git ' + repodir, { env: env });
        done();
    }

    function addFile(done) {
        var env = Object.create(process.env);
        env.GIT_SSH = __dirname + '/git_ssh_wrapper.sh';
        execSync('touch newfile && git add newfile && git commit -a -mx && git push ssh://git@' + app.fqdn + ':29418/' + gitlab_username + '/' + reponame + ' master',
                 { env: env, cwd: repodir });
        rimraf.sync('/tmp/testrepo');
        done();
    }

    function downloadTarball(done) {
        browser.manage().getCookies().then(function (cookies) {
            var cookie = cookies.filter(function (c) { return c.name === '_gitlab_session'; })[0];
            superagent.get('https://' + app.fqdn + '/' + gitlab_username + '/' + reponame + '/repository/archive.zip')
                .set('Cookie', cookie.name + '=' + cookie.value) // _gitlab_session
                .end(function (error, result) {
                expect(error).to.be(null);
                expect(result.statusCode).to.be(200);
                done();
            });
        });
    }

    function getMainPage(done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app without sso', function () {
        execSync('cloudron install --new --wait --no-sso --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can change admin credentials', setAdminCredentials);
    it('can login using new credentials', adminLogin.bind(null, "Sign in"));

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', getMainPage);
    it('can change admin credentials', setAdminCredentials);
    it('can login using new credentials', adminLogin.bind(null, "Standard"));

    it('can add public key', addPublicKey);

    it('can create repo', createRepo);
    it('displays correct ssh clone url', checkSshCloneUrl);
    it('displays correct https clone url', checkHttpCloneUrl);

    it('can add a README.md', addReadme);
    it('can clone the url', cloneUrl);
    it('can add and push a file', addFile);
    it('can download tarball', downloadTarball);
    it('can logout', logout);

    it('can login as cloudron user', checkLoginAsCloudronUser);

    it('can restart app', function (done) {
        execSync('cloudron restart --wait');
        done();
    });

    it('can clone the url', checkClone);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can clone the url', checkClone);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });


    it('can login using new credentials', adminLogin.bind(null, "Standard"));
    it('displays correct ssh clone url', checkSshCloneUrl);
    it('displays correct http clone url', checkHttpCloneUrl);

    it('can clone the url', checkClone);

    it('can logout', logout);

    it('can login as cloudron user', checkLoginAsCloudronUser);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --new --wait --appstore-id com.gitlab.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('can change admin credentials', setAdminCredentials);
    it('can login using new credentials', adminLogin.bind(null, "Standard"));

    it('can add public key', addPublicKey);

    it('can create repo', createRepo);
    it('can add a README.md', addReadme);
    it('can clone the url', cloneUrl);
    it('can add and push a file', addFile);
    it('can download tarball', downloadTarball);
 
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can clone the url', checkClone);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
