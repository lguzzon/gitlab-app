#!/bin/bash

set -eu

readonly GITLAB_CONFIG_DIR="/run/gitlab/config"

# SSH_PORT can be unset to disable SSH
if [[ -z "${SSH_PORT:-}" ]]; then
    echo "SSH disabled"
    SSH_PORT=29418 # arbitrary port to keep sshd happy
fi

# builds is the build logs and not artificats
mkdir -p /run/nginx /run/gitlab/ssh /run/gitlab/config/initializers /run/gitlab/log /run/gitlab/tmp /tmp/gitlab/repositories /run/gitlab/builds

mkdir -p /app/data/repositories /app/data/uploads /app/data/gitlab-satellites /app/data/backups/ /app/data/shared

# recreate the shared directory structure
cd /home/git/gitlab/shared && find . -type d -exec mkdir -p -- /app/data/shared/{} \;

sed -e "s/##POSTGRESQL_DATABASE/${POSTGRESQL_DATABASE}/" \
    -e "s/##POSTGRESQL_USERNAME/${POSTGRESQL_USERNAME}/" \
    -e "s/##POSTGRESQL_PASSWORD/${POSTGRESQL_PASSWORD}/" \
    -e "s/##POSTGRESQL_HOST/${POSTGRESQL_HOST}/" \
    -e "s/##POSTGRESQL_PORT/${POSTGRESQL_PORT}/" \
        /home/git/config_templates/database.yml > "${GITLAB_CONFIG_DIR}/database.yml"

sed -e "s,##REDIS_URL,redis://user:${REDIS_PASSWORD}@${REDIS_HOST}:${REDIS_PORT}," \
    /home/git/config_templates/resque.yml > "${GITLAB_CONFIG_DIR}/resque.yml"

sed -e "s/##APP_DOMAIN/${APP_DOMAIN}/g" \
    -e "s/##MAIL_FROM/${MAIL_FROM}/g" \
    -e "s/##REPLY_TO/noreply@${MAIL_DOMAIN}/g" \
    -e "s/##SSH_PORT/${SSH_PORT}/g" \
    -e "s/##MAIL_IMAP_SERVER/${MAIL_IMAP_SERVER}/g" \
    -e "s/##MAIL_IMAP_PORT/${MAIL_IMAP_PORT}/g" \
    -e "s/##MAIL_IMAP_USERNAME/${MAIL_IMAP_USERNAME}/g" \
    -e "s/##MAIL_IMAP_PASSWORD/${MAIL_IMAP_PASSWORD}/g" \
    -e "s/##MAIL_DOMAIN/${MAIL_DOMAIN}/g" \
    /home/git/config_templates/gitlab.yml > "${GITLAB_CONFIG_DIR}/gitlab.yml"

if [[ -n "${LDAP_SERVER:-}" ]]; then
    echo "Enabling LDAP integration"

    sed -e "s/##LDAP_ENABLED/true/g" \
        -e "s/##LDAP_SERVER/${LDAP_SERVER}/g" \
        -e "s/##LDAP_PORT/${LDAP_PORT}/g" \
        -e "s/##LDAP_BIND_DN/${LDAP_BIND_DN}/g" \
        -e "s/##LDAP_BIND_PASSWORD/${LDAP_BIND_PASSWORD}/g" \
        -e "s/##LDAP_USERS_BASE_DN/${LDAP_USERS_BASE_DN}/g" \
        -i "${GITLAB_CONFIG_DIR}/gitlab.yml"
else
    echo "Disabling LDAP integration"

    sed -e "s/##LDAP_ENABLED/false/g" -i "${GITLAB_CONFIG_DIR}/gitlab.yml"
fi

if [[ -f /app/data/gitlab.yml ]]; then
    echo "Using gitlab.yml custom config"
    cat /app/data/gitlab.yml >> "${GITLAB_CONFIG_DIR}/gitlab.yml"
fi

sed -e "s/##MAIL_SMTP_SERVER/${MAIL_SMTP_SERVER}/g" \
    -e "s/##MAIL_SMTP_PORT/${MAIL_SMTP_PORT}/g" \
    -e "s/##MAIL_SMTP_USERNAME/${MAIL_SMTP_USERNAME}/g" \
    -e "s/##MAIL_SMTP_PASSWORD/${MAIL_SMTP_PASSWORD}/g" \
    -e "s/##MAIL_DOMAIN/${MAIL_DOMAIN}/g" \
    /home/git/config_templates/smtp_settings.rb > "${GITLAB_CONFIG_DIR}/initializers/smtp_settings.rb"

if [[ ! -f "/app/data/sshd/ssh_host_ed25519_key" ]]; then
    echo "Generating ssh host keys"
    mkdir -p /app/data/sshd
    ssh-keygen -qt rsa -N '' -f /app/data/sshd/ssh_host_rsa_key
    ssh-keygen -qt dsa -N '' -f /app/data/sshd/ssh_host_dsa_key
    ssh-keygen -qt ecdsa -N '' -f /app/data/sshd/ssh_host_ecdsa_key
    ssh-keygen -qt ed25519 -N '' -f /app/data/sshd/ssh_host_ed25519_key
else
    echo "Reusing existing host keys"
fi

chmod 0600 /app/data/sshd/*_key
chmod 0644 /app/data/sshd/*.pub

sed -e "s/^Port .*/Port ${SSH_PORT}/" \
    -e "s/^#ListenAddress .*/ListenAddress 0.0.0.0/" \
    -e "s/UsePAM yes/UsePAM no/" \
    -e "s/UsePrivilegeSeparation yes/UsePrivilegeSeparation no/" \
    -e "\$aUseDNS no" \
    -e "s,^HostKey /etc/ssh/,HostKey /app/data/sshd/," \
    /etc/ssh/sshd_config > /run/gitlab/sshd_config

echo "Fixing secrets"
if [[ -f /app/data/secrets.yml ]]; then
    cp /app/data/secrets.yml "${GITLAB_CONFIG_DIR}/secrets.yml"
else
    sed -e "s/##DB_KEY_BASE/$(pwgen -1cns 30)/" \
        /home/git/config_templates/secrets.yml > "${GITLAB_CONFIG_DIR}/secrets.yml"
    cp "${GITLAB_CONFIG_DIR}/secrets.yml" /app/data/secrets.yml
fi

echo "Initializing gitlab shell"
sed -e "s/##REDIS_PORT/${REDIS_PORT}/g" \
    -e "s/##REDIS_HOST/${REDIS_HOST}/g" \
    -e "s/##REDIS_PASSWORD/${REDIS_PASSWORD}/g" \
    /home/git/config_templates/gitlab-shell.yml > /run/gitlab/gitlab-shell.yml

echo "Copying schema file"
cp /home/git/gitlab/db/schema_original.rb /run/gitlab/schema.rb

echo "Setting up tmp"
cp -fr /home/git/gitlab/tmp_original/* /run/gitlab/tmp

echo "Fixing permissions"
chown -R git:git /app/data /run/gitlab /tmp/gitlab
# https://blog.diacode.com/fixing-temporary-dir-problems-with-ruby-2
chmod o+t /tmp

echo "Creating pg_trgm extension"
if ! PGPASSWORD=${POSTGRESQL_PASSWORD} psql -q -h ${POSTGRESQL_HOST} -p ${POSTGRESQL_PORT} -U ${POSTGRESQL_USERNAME} -d ${POSTGRESQL_DATABASE} -c "CREATE EXTENSION IF NOT EXISTS pg_trgm;"; then
    echo "Failed to create extension pg_trgm"
    exit 1
fi

cd /home/git/gitlab
if [[ ! -f "/app/data/.dbsetup" ]]; then
    echo "Initializing db losing existing data"
    echo "yes" | sudo -u git bundle exec rake gitlab:setup RAILS_ENV=production
    sudo -u git bundle exec rake db:migrate RAILS_ENV=production
    touch /app/data/.dbsetup
else
    echo "Upgrading existing db"
    sudo -u git bundle exec rake db:migrate RAILS_ENV=production
fi

# For some reason, this tries to create repo directory
echo "Installing gitlab shell"
chmod u+rwx,g=rx,o-rwx /app/data/gitlab-satellites
sudo -u git /home/git/gitlab-shell/bin/install

# Recreate authorized_keys from database keys
echo "Synchronizing ssh keys"
echo "yes" | sudo -u git -H bundle exec rake gitlab:shell:setup RAILS_ENV=production

echo "Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i GitLab
