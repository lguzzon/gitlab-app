# Gitlab Cloudron App

This repository contains the Cloudron app package source for [Gitlab](http://gitlab.com/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=com.gitlab.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.gitlab.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd gitlab-app

cloudron build
cloudron install
```

## Updating

```
git diff v8.14.7:config/gitlab.yml.example v8.15.5:config/gitlab.yml.example
```

In gitlab shell:
```
git diff v4.0.3:config.yml.example v4.1.1:config.yml.example
```

Read through https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CHANGELOG.md
Gitlab releases are https://gitlab.com/gitlab-org/omnibus-gitlab/tags
Update notes https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/update/README.md

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the repos are still ok. The tests expect port 29418 to be available.

```
cd gitlab-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

