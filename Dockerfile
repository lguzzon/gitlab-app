FROM cloudron/base:0.10.0
MAINTAINER Girish Ramakrishnan <girish@cloudron.io>

# GitLab 10.x requires nginx 1.12.1 (https://gitlab.com/gitlab-org/gitlab-ce/issues/38301)
RUN curl -sS http://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - && \
    echo "deb http://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list && \
    add-apt-repository -y ppa:nginx/stable && \
    apt-get update && \
    apt-get install -y yarn libkrb5-dev libpq-dev openssh-server redis-tools nodejs libre2-dev git nginx && \
    rm -rf /etc/ssh_host_* && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN mkdir -p /usr/local/node-8.6.0
RUN curl -L https://nodejs.org/download/release/v8.6.0/node-v8.6.0-linux-x64.tar.gz  | tar zxf - --strip-components 1 -C /usr/local/node-8.6.0

ENV GOROOT /usr/local/go-1.7.5
ENV PATH /usr/local/node-8.6.0/bin:$GOROOT/bin:$PATH
# the gitaly build unsets GOROOT for some reason
RUN ln -s /usr/local/go-1.7.5 /usr/local/go

RUN adduser --disabled-login --gecos 'GitLab' git
# by default, git account is created as inactive which prevents login via openssh
# https://github.com/gitlabhq/gitlabhq/issues/5304
RUN passwd -d git

RUN gem install --no-document bundler

USER git
# Docker doesn't set the HOME automatically based on USER
ENV HOME /home/git
RUN git config --global user.name "GitLab"
RUN git config --global user.email "git@gitlab.com"
RUN git config --global core.autocrlf input

## get gitlab
RUN mkdir /home/git/gitlab
RUN wget $(curl -k -s https://api.github.com/repos/gitlabhq/gitlabhq/tags | grep tarball_url | grep -v "\.pre" | grep -v "\-rc" | head -n1 | cut -d '"' -f 4) -O - | tar -zxvf - --strip=1 -C /home/git/gitlab/

WORKDIR /home/git/gitlab
RUN bundle install --deployment --without development test mysql aws krb5
RUN yarn install --production --pure-lockfile

## get gitlab-shell
RUN mkdir /home/git/gitlab-shell
RUN wget https://github.com/gitlabhq/gitlab-shell/archive/v$(cat /home/git/gitlab/GITLAB_SHELL_VERSION).tar.gz -O - | tar -zxvf - --strip=1 -C /home/git/gitlab-shell/
RUN ln -s /run/gitlab/ssh /home/git/.ssh
RUN ln -s /run/gitlab/gitlab-shell.yml /home/git/gitlab-shell/config.yml
RUN /home/git/gitlab-shell/bin/compile

# get gitlab-workhorse
RUN mkdir /home/git/gitlab-workhorse
RUN curl -L "https://gitlab.com/gitlab-org/gitlab-workhorse/repository/archive.tar.gz?ref=v$(cat /home/git/gitlab/GITLAB_WORKHORSE_VERSION)" | tar -zxvf - --strip=1 -C /home/git/gitlab-workhorse
RUN cd /home/git/gitlab-workhorse && make

# get gitaly
RUN mkdir /home/git/gitaly
RUN curl -L "https://gitlab.com/gitlab-org/gitaly/repository/archive.tar.gz?ref=v$(cat /home/git/gitlab/GITALY_SERVER_VERSION)" | tar -zxvf - --strip=1 -C /home/git/gitaly
RUN cd /home/git/gitaly && make

## compile assets (dummy configs are required). this step require gitlab-shell/VERSION to exist
RUN cp /home/git/gitlab/config/database.yml.postgresql /home/git/gitlab/config/database.yml && \
    cp /home/git/gitlab/config/gitlab.yml.example /home/git/gitlab/config/gitlab.yml && \
    cp /home/git/gitlab/config/resque.yml.example /home/git/gitlab/config/resque.yml && \
    bundle exec rake gettext:compile RAILS_ENV=production SKIP_STORAGE_VALIDATION=true && \
    bundle exec rake assets:clean assets:precompile webpack:compile RAILS_ENV=production SETUP_DB=false USE_DB=false SKIP_STORAGE_VALIDATION=true && \
    rm /home/git/gitlab/config/database.yml /home/git/gitlab/config/gitlab.yml /home/git/gitlab/config/resque.yml

# RUN sed -e "s/# config.logger.*/config.logger = Logger.new(STDOUT)/" -i /home/git/gitlab/config/environments/production.rb
# builds is the build logs and not the artifacts of the builds
RUN rm -rf /home/git/gitlab/log && ln -s /run/gitlab/log /home/git/gitlab/log && \
    rm -rf /home/git/gitlab/builds && ln -s /run/gitlab/builds /home/git/gitlab/builds

# hack to make schema.rb writable by active record
RUN cp /home/git/gitlab/db/schema.rb /home/git/gitlab/db/schema_original.rb && \
    ln -sf /run/gitlab/schema.rb /home/git/gitlab/db/schema.rb

## Configuration
RUN cp /home/git/gitlab/config/unicorn.rb.example /home/git/gitlab/config/unicorn.rb
RUN cp /home/git/gitlab/config/initializers/rack_attack.rb.example /home/git/gitlab/config/initializers/rack_attack.rb
RUN ln -s /run/gitlab/config/database.yml /home/git/gitlab/config/database.yml && \
    ln -s /run/gitlab/config/resque.yml /home/git/gitlab/config/resque.yml && \
    ln -s /run/gitlab/config/gitlab.yml /home/git/gitlab/config/gitlab.yml && \
    ln -s /run/gitlab/config/initializers/smtp_settings.rb /home/git/gitlab/config/initializers/smtp_settings.rb && \
    ln -sf /app/data/secrets.yml /home/git/gitlab/config/secrets.yml

# config/initializers/gitlab_shell_secret_token.rb creates a symlink, so this has to exist
RUN ln -sf /run/gitlab/gitlab_shell_secret /home/git/gitlab-shell/.gitlab_shell_secret && \
    ln -sf /run/gitlab/gitlab_workhorse_secret /home/git/gitlab/.gitlab_workhorse_secret

## Setup tmp
RUN mv /home/git/gitlab/tmp /home/git/gitlab/tmp_original && \
    ln -s /run/gitlab/tmp /home/git/gitlab/tmp

## used for generating tarball
RUN ln -s /tmp/gitlab /home/git/gitlab/shared/cache

RUN rm -rf /home/git/gitlab/public/uploads && ln -s /app/data/uploads /home/git/gitlab/public/uploads

# Add configuration template
ADD config_templates /home/git/config_templates

# add nginx config
USER root
RUN rm /etc/nginx/sites-enabled/*
RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log
ADD nginx_readonlyrootfs.conf /etc/nginx/conf.d/readonlyrootfs.conf
RUN ln -s /etc/nginx/sites-available/gitlab /etc/nginx/sites-enabled/gitlab
RUN cp /home/git/gitlab/lib/support/nginx/gitlab /etc/nginx/sites-available/gitlab
RUN sed -e '/YOUR_SERVER_FQDN/d' \
        -e 's,.*access_log.*,access_log /dev/stdout;,' \
        -e 's,.*error_log.*,error_log /dev/stderr info;,' \
        -e 's,.*proxy_set_header.*X-Forwarded-Proto.*$scheme;,proxy_set_header X-Forwarded-Proto https; proxy_set_header X-Forwarded-Ssl on;,' \
        -i /etc/nginx/sites-available/gitlab

# add supervisor configs
ADD supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/gitlab/supervisord.log /var/log/supervisor/supervisord.log

ADD start.sh /home/git/start.sh

CMD [ "/home/git/start.sh" ]

