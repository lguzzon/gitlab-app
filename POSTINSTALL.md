#### User management

GitLab optionally integrates with Cloudron user management. When enabled,
Cloudron users can login to GitLab using the `Cloudron` tab in the login screen.

On startup, you will be asked to set the `root` user password. Using this `root` account,
you can make other users as GitLab admins.

**IMPORTANT**: Remember to change the email address of the root account, so you can reset
the password easily if required.


