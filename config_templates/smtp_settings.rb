# To enable smtp email delivery for your GitLab instance do next: 
# 1. Rename this file to smtp_settings.rb
# 2. Edit settings inside this file
# 3. Restart GitLab instance
#
if Rails.env.production?
  Rails.application.config.action_mailer.delivery_method = :smtp

  ActionMailer::Base.delivery_method = :smtp
  ActionMailer::Base.smtp_settings = {
    address: "##MAIL_SMTP_SERVER",
    port: ##MAIL_SMTP_PORT,
    domain: "##MAIL_DOMAIN",
    user_name: '##MAIL_SMTP_USERNAME',
    password: '##MAIL_SMTP_PASSWORD',
    authentication: 'login'
  }
end
