This app packages GitLab 10.3.1

GitLab Community Edition (CE) includes git repository management, code reviews, 
an issue tracking and wiki’s and [much more](https://about.gitlab.com/features/).

## Features
* Manage Git repositories with fine grained access controls that keep your code secure
* Perform code reviews and enhance collaboration with merge requests
* Each project can also have an issue tracker and a wiki
* Used by more than 100,000 organizations, GitLab is the most popular solution to manage Git repositories on-premises
* Completely free and open source (MIT Expat license)
* Powered by Ruby on Rails

## Website

On [about.gitlab.com](https://about.gitlab.com/) you can find more information about:

- [Subscriptions](https://about.gitlab.com/subscription/)
- [Consultancy](https://about.gitlab.com/consultancy/)
- [Community](https://about.gitlab.com/community/)
- [Hosted GitLab.com](https://about.gitlab.com/gitlab-com/) use GitLab as a free service
- [GitLab Enterprise Edition](https://about.gitlab.com/gitlab-ee/) with additional features aimed at larger organizations.
- [GitLab CI](https://about.gitlab.com/gitlab-ci/) a continuous integration (CI) server that is easy to integrate with GitLab.

## Getting help

Please see [Getting help for GitLab](https://about.gitlab.com/getting-help/) on our website for the many options to get help.

## Is it any good?

[Yes](https://news.ycombinator.com/item?id=3067434)

## Is it awesome?

Thanks for [asking this question](https://twitter.com/supersloth/status/489462789384056832) Joshua.
[These people](https://twitter.com/gitlab/favorites) seem to like it.

